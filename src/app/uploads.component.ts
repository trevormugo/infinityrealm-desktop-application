import { Component } from '@angular/core';
import { MultiService } from './multi.service';
import { HttpClient ,HttpParams ,HttpHeaders} from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from "@angular/router";
import { OnInit , ViewChild } from "@angular/core";
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatTable } from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'uploads-page',
  templateUrl: './uploads.component.html',
  styleUrls: ['./uploads.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class UploadsComponent implements OnInit{
  records:any = [];

  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;

  constructor(public router : Router ,public multiserve : MultiService){}

  ngOnInit(){
    //subscribe to a service here
    this.dummyfunc();
  }
  displayedColumns: string[] = ['_id','fullName','userName','email','coins','phoneNumber','password','profilepic','verified'];
  dataSource;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  public dummyfunc(){
    this.multiserve.accountsReq()
      .subscribe((data) => {
                      if(data.error)
                      {
                        //no need for the .error since its the only response with an identifier of error;
                        //an error occured in finding the information from the database
                        alert(" Error : we might be experiencing some technical difficulties try again in a bit");
                      }
                      else
                      {
                        //POSSIBLE ERRORS
                        //email exists and the password is correct
                        this.records = data;
                        this.dataSource = new MatTableDataSource(this.records);
                        this.dataSource.sort = this.sort;
                      }
                  },
                  (error) => {
                    //an error occurred
                    alert(" Error : we might be experiencing some technical difficulties try again in a bit");
                  }
            );
  }
  
}
