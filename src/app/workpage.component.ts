import { Component } from '@angular/core';
import { LoginService } from './login.service';
import { HttpClient ,HttpParams ,HttpHeaders} from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from "@angular/router";
import { OnInit } from "@angular/core";
import {Subject} from "rxjs";
import { Adress } from "./ip.service";
import { DOCUMENT } from '@angular/common';
import { Inject }  from '@angular/core';
import { FileSelectDirective , FileUploader } from "ng2-file-upload";

@Component({
  selector: 'work-page',
  templateUrl: './workpage.component.html',
  styleUrls: ['./workpage.component.css']
})
export class WorkComponent implements OnInit{
  public value;
  public mysub = new Subject<any>();
  public myaud;
  public sliderval;
  public errorcomp:String;
  public errorlanding;
  private ip=this.myip.getIp()+"/fileuploadbeats";
  uploader:FileUploader = new FileUploader({
    url : this.ip,
    disableMultipart : false,
  });
  constructor(@Inject(DOCUMENT) document ,public router : Router ,public loginserve : LoginService , public myip : Adress){
    this.uploader.onCompleteItem = (item:any , response:any , status:any , headers:any)=>{

    }
  }

  ngOnInit(){
    let audio = new Audio();
    this.uploader.onBeforeUploadItem = (item) => {
      item.withCredentials = false;
    }
    this.mysub.subscribe((data) => {
      console.log(data);
      if(data.mimetype === 'audio/mp3' || data.mimetype === 'audio/ogg' || data.mimetype === 'audio/wav' || data.mimetype === 'audio/aac')
      {
        let location;
        if(data.from === "beats")
        {
          location = this.myip.getIp()+'/beatsreader/'+data.id;
        }
        else
        {
          location = this.myip.getIp()+'/reader/'+data.id;
        }
        try{
          audio.pause();
          data.ref.pause();
        }catch(error){
          console.log("cant do that now");
        }
        audio.ontimeupdate=()=>{
            this.sliderval = audio.currentTime /  audio.duration * 100;
        };
        //access the audio object in the class
        this.myaud = audio;
        audio.crossOrigin = "anonymous";
        audio.src = location;
        audio.controls = false;
        //we will set this later
        audio.loop = false;
        audio.autoplay = true;
        audio.load();
        audio.play();
      }
      else
      {
        try{
          audio.pause();
          data.ref.pause();
        }catch(error){
          console.log("cant do that now");
        }
        data.ref.src =this.myip.getIp()+'/reader/'+data.id;
        data.ref.crossOrigin = "anonymous";
        data.ref.controls = false;
        data.ref.play();
        data.ref.ontimeupdate=()=>{
            this.sliderval = data.ref.currentTime /  data.ref.duration * 100;
        };
      }
    },
    (error) => {
    //an error occurred
      alert(" Error : we might be experiencing some technical difficulties try again in a bit");
      }
    );
  }
  public query($event){
    this.value = $event.target.value;
  }
  public queryfunc(){
    this.value = "";
  }

}
