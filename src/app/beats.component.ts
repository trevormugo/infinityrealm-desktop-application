import { Component } from '@angular/core';
import { MultiService } from './multi.service';
import { HttpClient ,HttpParams ,HttpHeaders} from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from "@angular/router";
import { OnInit , ViewChild } from "@angular/core";
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatTable } from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Adress } from "./ip.service";
import { WorkComponent } from "./workpage.component";
import { DOCUMENT } from '@angular/common';
import { Inject }  from '@angular/core';
@Component({
  selector: 'beats-page',
  templateUrl: './beats.component.html',
  styleUrls: ['./beats.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class BeatsComponent implements OnInit{
  records:any = [];
  public imported;
  public check = Observable.create((observer)=>{
    this.t = setInterval(()=>{
      observer.next(this.workpage.sliderval);
    },1000);
    if(this.workpage.sliderval === 100){
      observer.complete();
    }
  });
  public ans;
  public t;
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;


  constructor(@Inject(DOCUMENT) document , public workpage : WorkComponent , public myip : Adress , public router : Router ,public multiserve : MultiService){}

  ngOnInit(){
    //subscribe to a service here
    this.dummyfunc();
  }
  displayedColumns: string[] = ['_id','mimetype','filename','timestamp','size'];
  dataSource;
  public isplaying:Boolean = true;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  public dummyfunc(){
    this.multiserve.beatsReq()
      .subscribe((data) => {
                      if(data.error)
                      {
                        //no need for the .error since its the only response with an identifier of error;
                        //an error occured in finding the information from the database
                        alert(" Error : we might be experiencing some technical difficulties try again in a bit");
                      }
                      else
                      {
                        //POSSIBLE ERRORS
                        //email exists and the password is correct
                        this.records = data;
                        this.dataSource = new MatTableDataSource(this.records);
                        this.dataSource.sort = this.sort;
                      }
                  },
                  (error) => {
                    //an error occurred
                    alert(" Error : we might be experiencing some technical difficulties try again in a bit");
                  }
            );
  }
  public expandandretract(args){
    this.expandedElement = this.expandedElement === args ? null : args;console.log(args);
    //prevents multiple instances of audio
    if(this.expandedElement === args)
    {
      this.check.subscribe((data)=>{
        this.imported = data;
        if(this.workpage.sliderval === 100){
          clearInterval(this.t);
        }
      },(error)=>{console.log(error)});
      this.workpage.mysub.next({id : args._id , mimetype : args.mimetype , from : "beats"});
    }
    else
    {
      this.workpage.myaud.pause();
      clearInterval(this.t);
    }
  }
  public pauseplay(){
    if(this.isplaying === true)
    {
      this.workpage.myaud.pause();
      this.isplaying = false;
    }
    else
    {
      this.workpage.myaud.play();
      this.isplaying = true;
    }
  }

  public mouse($event){
    this.ans = this.workpage.myaud.duration * ($event.value / 100);
    this.workpage.myaud.currentTime = this.ans;
    console.log($event.value);
  }
  public deletefunc(args){
    var datatosend = {
      id : args,
    }
    this.multiserve.deletereqbeat(datatosend)
      .subscribe((data) => {
                 if(data.error)
                 {
                  //an errro occured;
                  alert("an error occured");
                 }
                 else
                 {
                   var index = this.records.findIndex(item => item._id === args);
                   console.log(index);this.records.splice(index);
                   this.table.renderRows();
                 }
      },(error) => {
        //an error occurred
        alert(" Error : we might be experiencing some technical difficulties try again in a bit");
        }
      );
  }
}
