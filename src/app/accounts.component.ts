import { Component } from '@angular/core';
import { MultiService } from './multi.service';
import { HttpClient ,HttpParams ,HttpHeaders} from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from "@angular/router";
import { OnInit , ViewChild } from "@angular/core";
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatTable } from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Adress } from "./ip.service";
import { WorkComponent } from "./workpage.component";
import { DOCUMENT } from '@angular/common';
import { Inject }  from '@angular/core';

@Component({
  selector: 'accounts-page',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AccountsComponent implements OnInit{
  records:any = [];
  public mysrc:String=this.myip.getIp()+"/coverimage/";
  public imported;
  public check = Observable.create((observer)=>{
    this.t = setInterval(()=>{
      observer.next(this.workpage.sliderval);
    },1000);
    if(this.workpage.sliderval === 100){
      observer.complete();
    }
  });
  public ans;
  public t;
  public video;
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;


  constructor(@Inject(DOCUMENT) document , public workpage : WorkComponent , public myip : Adress , public router : Router ,public multiserve : MultiService){}

  ngOnInit(){
    //subscribe to a service here
    this.dummyfunc();
  }
  displayedColumns: string[] = ['_id','filepic','mimetype','filename','uploader','timestamp','size','display'];
  dataSource;
  public isplaying:Boolean = true;
  public vidisplaying:Boolean = true;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  public dummyfunc(){
    this.multiserve.uploadsReq()
      .subscribe((data) => {
                      if(data.error)
                      {
                        //no need for the .error since its the only response with an identifier of error;
                        //an error occured in finding the information from the database
                        alert(" Error : we might be experiencing some technical difficulties try again in a bit");
                      }
                      else
                      {
                        //POSSIBLE ERRORS
                        //email exists and the password is correct
                        this.records = data;
                        this.dataSource = new MatTableDataSource(this.records);
                        this.dataSource.sort = this.sort;
                      }
                  },
                  (error) => {
                    //an error occurred
                    alert(" Error : we might be experiencing some technical difficulties try again in a bit");
                  }
            );
  }
  public expandandretract(args){
    this.expandedElement = this.expandedElement === args ? null : args;
    //prevents multiple instances of audio
    if(this.expandedElement === args)
    {
      this.check.subscribe((data)=>{
        this.imported = data;
        if(this.workpage.sliderval === 100){
          clearInterval(this.t);
        }
      },(error)=>{console.log(error)});
      if(args.mimetype === 'audio/mp3' || args.mimetype === 'audio/ogg' || args.mimetype === 'audio/wav' || args.mimetype === 'audio/aac')
      {
        this.workpage.mysub.next({id : args._id ,ref : this.video, mimetype : args.mimetype , from : "uploads"});
        try{
          document.getElementById(args.timestamp).remove();
        }catch(error){console.log(error)}
      }
      else
      {
        try{
          document.getElementById(args.timestamp).remove();
          this.video.pause();
        }catch(error){console.log("not now")}
        this.video = document.createElement("video");
        this.video.setAttribute("class", "myvideo");
        this.video.setAttribute("id", args.timestamp);
        document.getElementById(args._id).appendChild(this.video);
        this.workpage.mysub.next({id : args._id,ref : this.video, mimetype : args.mimetype});
      }
    }
    else
    {
      if(args.mimetype === 'audio/mp3' || args.mimetype === 'audio/ogg' || args.mimetype === 'audio/wav' || args.mimetype === 'audio/aac')
      {
        this.workpage.myaud.pause();
        clearInterval(this.t);
      }
      else
      {
        this.video.pause();
        clearInterval(this.t);
        document.getElementById(args.timestamp).remove();
      }
    }
  }
  public pauseplay(){
    if(this.isplaying === true)
    {
      this.workpage.myaud.pause();
      this.isplaying = false;
    }
    else
    {
      this.workpage.myaud.play();
      this.isplaying = true;
    }
  }
  public vidpauseplay(){
    if(this.vidisplaying === true)
    {
      this.video.pause();
      this.vidisplaying = false;
    }
    else
    {
      this.video.play();
      this.vidisplaying = true;
    }
  }
  public mouse($event){
    this.ans = this.workpage.myaud.duration * ($event.value / 100);
    this.workpage.myaud.currentTime = this.ans;
    console.log($event.value);
  }
  public vidmouse($event){
    this.ans = this.video.duration * ($event.value / 100);
    this.video.currentTime = this.ans;
    console.log($event.value);
  }
  public deletefunc(args){
    var datatosend = {
      id : args.id,
    }
    this.multiserve.deletereq(datatosend)
      .subscribe((data) => {

                  },
                  (error) => {
                    //an error occurred
                    alert(" Error : we might be experiencing some technical difficulties try again in a bit");
                  }
            );
  }
}
