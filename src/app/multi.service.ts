import { Injectable } from "@angular/core";
import { HttpClient , HttpHeaders , HttpErrorResponse} from "@angular/common/http";
import { Observable , throwError } from "rxjs";
import { retry ,catchError } from "rxjs/operators";
import { Adress } from "./ip.service";
@Injectable()
export class MultiService {
  url = this.outurl.getIp();
  constructor( public http : HttpClient , public outurl : Adress){}
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent)
      {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      }
      else
      {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
      }
         // return an observable with a user-facing error message
      return throwError('Something bad happened; please try again later.');
  };
  uploadsReq():Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.get<any>(this.url + "/adminrequploads",httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  accountsReq():Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.get<any>(this.url + "/adminreqaccounts",httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  likesReq():Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.get<any>(this.url + "/adminreqlikes",httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  followsReq():Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.get<any>(this.url + "/adminreqfollows",httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  coinsReq():Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.get<any>(this.url + "/adminreqcoins",httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  beatsReq():Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.get<any>(this.url + "/adminreqbeats",httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  savedReq():Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.get<any>(this.url + "/adminreqsaved",httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  locationReq():Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.get<any>(this.url + "/adminreqlocations",httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  notificationsReq():Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.get<any>(this.url + "/adminreqnotif",httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  deletereq(deleteinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/adminuploaddeletereq",deleteinfo,httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  deletereqbeat(deleteinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/adminbeatdeletereq",deleteinfo,httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  deletereqcoins(deleteinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/admincoinsdeletereq",deleteinfo,httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  deletereqfollows(deleteinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/adminfollowdeletereq",deleteinfo,httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  deletereqlikes(deleteinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/adminlikesdeletereq",deleteinfo,httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  deletereqlocations(deleteinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/adminlocationsdeletereq",deleteinfo,httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  deletereqnotifications(deleteinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/adminnotificationsdeletereq",deleteinfo,httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  deletereqsaved(deleteinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/adminsaveddeletereq",deleteinfo,httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  deletereqaccounts(deleteinfo):Observable <any> {
    const httpOptions = {
         headers : new HttpHeaders({
            "Content-Type":  "application/json",
            "Authorization": "trevor",
            "Access-Control-Allow-Origin" : "*"
         })
     }
    return this.http.post<any>(this.url + "/adminaccountdeletereq",deleteinfo,httpOptions)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
}
