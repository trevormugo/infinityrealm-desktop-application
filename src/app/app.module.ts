import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LoginService } from './login.service';
import { Adress } from './ip.service';
import { HttpClientModule } from '@angular/common/http';
import {MatTabsModule} from '@angular/material/tabs';
import {MatInputModule} from '@angular/material/input';
import { AccountsComponent } from './accounts.component';
import { BlacklistedComponent } from './blacklisted.component';
import { CoinsComponent } from './coins.component';
import { FakereportsComponent } from './fakereports.component';
import { FollowsComponent } from './follows.component';
import { LikesComponent } from './likes.component';
import { PreservedComponent } from './preserved.component';
import { ReportsComponent } from './reports.component';
import { UploadsComponent } from './uploads.component';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import { MultiService } from './multi.service';
import { BeatsComponent } from './beats.component';
import { SavedComponent } from './saved.component';
import { LocationComponent } from './location.component';
import { NotificationComponent } from './notifications.component';
import { FileUploadModule } from 'ng2-file-upload';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WorkComponent } from './workpage.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CdkTableModule} from '@angular/cdk/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSliderModule} from '@angular/material/slider';

@NgModule({
  declarations: [
    AppComponent,
    WorkComponent,
    AccountsComponent,
    BlacklistedComponent,
    CoinsComponent,
    FakereportsComponent,
    FollowsComponent,
    LikesComponent,
    PreservedComponent,
    ReportsComponent,
    UploadsComponent,
    SavedComponent,
    BeatsComponent,
    LocationComponent,
    NotificationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    CdkTableModule,
    MatSliderModule,
    FileUploadModule,
  ],
  providers: [
    LoginService,
    Adress,
    MultiService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
